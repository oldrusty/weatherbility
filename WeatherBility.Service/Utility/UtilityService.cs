﻿using System;
using System.Collections.Concurrent;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using WeatherBility.Data;

namespace WeatherBility.Service.Utility
{
    public interface IUtiliyService
    {
        bool IsInputValid(string input);
        HttpClient ConstructRequest(string city, IConfigurationRoot configuration);
        Task<string> GetWeatherDetail(HttpClient client);
        bool IsResponseValid(string response);
        WeatherData DeserializeResponse(string response);
    }

    public class UtilityService : IUtiliyService
    {
        private const string GETWEATHERFAILCODE = "GETWEATHERFAIL";
        ConcurrentDictionary<string, WeatherData> lookUpDictionary;

        public UtilityService()
        {
            lookUpDictionary = new ConcurrentDictionary<string, WeatherData>();
        }

        public HttpClient ConstructRequest(string city, IConfigurationRoot configuration)
        {
            HttpClient client = new HttpClient();

            var builder = new UriBuilder(configuration.GetSection("weatherUrl").Value);
            var query = HttpUtility.ParseQueryString(builder.Query);
            query["q"] = city;
            query["APPID"] = configuration.GetSection("appId").Value;
            builder.Query = query.ToString();
            client.BaseAddress = new Uri(builder.ToString());

            return client;
        }

        public async Task<string> GetWeatherDetail(HttpClient client)
        {
            HttpResponseMessage response = await client.GetAsync(client.BaseAddress.AbsoluteUri);
            return response.IsSuccessStatusCode
                ? await response.Content.ReadAsStringAsync()
                : $"{GETWEATHERFAILCODE}: Unable to retrieve your request. StatusCode: {response.StatusCode}. " +
                    $"Response: {response.ReasonPhrase}";
        }

        public bool IsResponseValid(string response)
        {
            return !string.IsNullOrEmpty(response) &&
                !response.StartsWith(GETWEATHERFAILCODE, StringComparison.Ordinal);
        }

        public WeatherData DeserializeResponse(string response)
        {
            return JsonConvert.DeserializeObject<WeatherData>(response);
        }

        public bool IsInputValid(string input)
        {
            return !string.IsNullOrEmpty(input) &&
                System.Text.RegularExpressions.Regex.IsMatch(input, @"^[a-zA-Z]+(\s[a-zA-Z]+)?$");
        }
    }
}
