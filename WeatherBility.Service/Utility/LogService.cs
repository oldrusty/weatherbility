﻿using System;

namespace WeatherBility.Service.Utility
{
    public interface ILogService
    {
        void LogMessage(string message);
        void LogAlertMessage(string message);
        void LogNotificationMessage(string message);
        void LogWarningMessage(string message);
        void LogErrorMessage(string message);
    }

    public class LogService : ILogService
    {
        public void LogAlertMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White; ;
        }

        public void LogErrorMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void LogMessage(string message)
        {
            Console.WriteLine(message);
        }

        public void LogNotificationMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void LogWarningMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
