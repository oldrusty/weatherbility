﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WeatherBility.Data;
using WeatherBility.Service.Utility;

namespace WeatherBility.Service.Core
{
    public interface IWeatherService
    {
        void ProcessGetWeather(string city);
        bool IsRequestProcessedPreviously(string city);
        Task GetWeather(string city);
        bool ValidateInput(string input);
        void DisplayWeather(WeatherData weatherData, bool isCachedData = false);
        ICollection<WeatherData> DisplaySavedData(ICollection<WeatherData> savedData);
        void DisplayMessage(string message);
        void DisplayAvailableCommands();
    }

    public class WeatherService : IWeatherService
    {
        public const string GETWEATHERFAILCODE = "GETWEATHERFAIL";
        // this is the where the result will be store for quick access. This acts as the repo layer
        ConcurrentDictionary<string, WeatherData> lookUpDictionary;
        readonly IConfigurationRoot configuration;
        readonly IUtiliyService utilityService;
        readonly ILogService logService;

        public WeatherService(
            IConfigurationRoot configuration
            , IUtiliyService utilityService
            , ILogService logService)
        {
            lookUpDictionary = new ConcurrentDictionary<string, WeatherData>();
            this.configuration = configuration;
            this.utilityService = utilityService;
            this.logService = logService;
        }

        public void ProcessGetWeather(string city)
        {
            if (IsRequestProcessedPreviously(city)) return;
            Task.Factory.StartNew(() => GetWeather(city));
        }

        public bool IsRequestProcessedPreviously(string city)
        {
            if (lookUpDictionary.ContainsKey(city))
            {
                if (lookUpDictionary.TryGetValue(city, out WeatherData weatherData) && weatherData != null)
                {
                    logService.LogAlertMessage($"This city {city} weather was previously retrieved and saved. " +
                        "Here's the cached result");
                    DisplayWeather(weatherData, true);
                    return true;
                }
            }
            return false;
        }

        public async Task GetWeather(string city)
        {
            try
            {
                logService.LogNotificationMessage($"Retriving weather for \'{city}\' asynchronously. " +
                    $"You may enter in another city while it's fetching");
                var client = utilityService.ConstructRequest(city, configuration);
                var response = await utilityService.GetWeatherDetail(client);

                if (utilityService.IsResponseValid(response))
                {
                    var weather = utilityService.DeserializeResponse(response);
                    lookUpDictionary.AddOrUpdate(weather.name.ToLower(), weather, (key, oldValue) => oldValue);
                    Console.WriteLine(weather.ToString());
                }
                else
                {
                    logService.LogErrorMessage(response);
                }
            }
            catch (Exception ex)
            {
                logService.LogErrorMessage($"Exception caught. Message: {ex.Message}");
            }
        }

        public bool ValidateInput(string input)
        {
            var isInputValid = utilityService.IsInputValid(input);
            if (!isInputValid)
            {
                logService.LogWarningMessage($"Invalid user input \'{input}\'");
            }
            return isInputValid;
        }

        public void DisplayWeather(WeatherData weatherData, bool isCachedData = false)
        {
            if (!isCachedData)
            {
                logService.LogAlertMessage($"New weather received!");
            }
            logService.LogMessage(weatherData.ToString());
        }

        public bool CheckAndProcessForCommand(string input)
        {
            var isInputACommand = false;
            switch (input)
            {
                case "h":
                    logService.LogMessage(AvailableCommands);
                    isInputACommand = true;
                    break;
                case "s":
                    DisplaySavedData(GetSavedCities());
                    isInputACommand = true;
                    break;
                case "c":
                    Console.Clear();
                    isInputACommand = true;
                    break;
                case "e":
                    isInputACommand = true;
                    logService.LogNotificationMessage("Exiting... Press any key to exit");
                    break;
            }
            return isInputACommand;
        }

        public string AvailableCommands =>
            $"**************************************************************\n" +
            $"Commands\tDescription\n" +
            $"h\t\tDisplay commands\n" +
            $"s\t\tShow all saved city\n" +
            $"c\t\tClear screen\n" +
            $"e\t\tExit program(note this will clear your saved data)\n" +
            $"**************************************************************\n";

        public ICollection<WeatherData> DisplaySavedData(ICollection<WeatherData> savedData)
        {
            if (savedData?.Count < 1)
            {
                logService.LogNotificationMessage("No saved record");
                return null;
            }

            foreach (var data in savedData)
            {
                DisplayWeather(data, true);
            }

            return savedData;
        }

        public ICollection<WeatherData> GetSavedCities()
        {
            return lookUpDictionary.Values;
        }

        public void DisplayMessage(string message) => logService.LogMessage(message);

        public void DisplayAvailableCommands() => logService.LogMessage(AvailableCommands);
    }
}
