﻿using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System.Threading.Tasks;
using WeatherBility.Service.Core;
using WeatherBility.Service.Utility;

namespace WeatherBility.Test
{
    [TestFixture]
    public class UtilityServiceTest
    {
        private readonly UtilityService utilityService;
        private readonly IConfigurationRoot configuration;

        public UtilityServiceTest()
        {
            utilityService = new UtilityService();
            configuration = new ConfigurationBuilder()
                .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
        }

        [Test]
        public void ConstructRequest_Creates_Client_With_Configuration()
        {
            var inputCity = "foo";

            var httpClient = utilityService.ConstructRequest(inputCity, configuration);

            Assert.IsNotNull(httpClient);
            Assert.AreEqual(httpClient.BaseAddress.AbsoluteUri,
                $"{ configuration.GetSection("weatherUrl").Value}?q={inputCity}&APPID={configuration.GetSection("appId").Value}");
        }

        [Test]
        public void GetWeatherDetail_For_A_City_That_Does_Not_Exists()
        {
            var invalidCity = "foo bar 123";

            var httpClient = utilityService.ConstructRequest(invalidCity, configuration);

            var actualResponse = "";
            Task.Run(async () =>
            {
                actualResponse = await utilityService.GetWeatherDetail(httpClient);
            }).GetAwaiter().GetResult();

            Assert.IsFalse(string.IsNullOrEmpty(actualResponse));
            Assert.IsTrue(actualResponse.Contains(WeatherService.GETWEATHERFAILCODE));
        }

        [Test]
        public void IsResponseValid_Returns_False_When_Empty_Response_Is_Provided()
        {
            var invalidResponse = string.Empty;

            Assert.IsFalse(utilityService.IsResponseValid(invalidResponse));
        }

        [Test]
        public void IsInputValid_Returns_False_When_Input_With_Numbers_Is_Provided()
        {
            var inputWithNumber = $"Perth1";

            Assert.IsFalse(utilityService.IsInputValid(inputWithNumber));
        }

        [Test]
        public void IsInputValid_Returns_True_When_Input_Has_Space()
        {
            var inputWithSpace = $"Manchester Center";

            Assert.IsTrue(utilityService.IsInputValid(inputWithSpace));
        }
    }
}
