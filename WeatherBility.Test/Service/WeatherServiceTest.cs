﻿using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using WeatherBility.Service.Core;
using WeatherBility.Service.Utility;

namespace WeatherBility.Test
{
    [TestFixture]
    public class WeatherServiceTest
    {
        private readonly WeatherService weatherService;

        public WeatherServiceTest()
        {
            weatherService = new WeatherService(
                new ConfigurationBuilder()
                    .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build()
                , new UtilityService()
                , new LogService());
        }

        [Test]
        public void ProcessGetWeather_MutipleRequests()
        {
            var validCityLists = new List<string> { "Perth", "Sydney", "Brisbane", "London", "Melbourne" };

            validCityLists.ForEach(c => weatherService.ProcessGetWeather(c));

            Thread.Sleep(5000); // time for the service to process all requests

            var savedCities = weatherService.GetSavedCities();

            Assert.AreEqual(savedCities.Count, validCityLists.Count);
            foreach (var city in savedCities)
            {
                Assert.AreEqual(city.name, validCityLists.FirstOrDefault(c => c == city.name));
            }
        }

        [Test]
        public void CheckAndProcessForCommand_For_Command_And_Space()
        {
            var notACommand = "h" + " ";
            Assert.IsFalse(weatherService.CheckAndProcessForCommand(notACommand));
        }
    }
}
