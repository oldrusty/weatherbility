﻿using System;
using Microsoft.Extensions.Configuration;
using WeatherBility.Service.Core;
using WeatherBility.Service.Utility;

namespace WeatherBility
{
    class Program
    {
        static void Main()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var service = new WeatherService(
                configuration
                , new UtilityService()
                , new LogService());

             
            service.DisplayMessage("WeatherBility! Gets weather by city name asynchronously");
            service.DisplayAvailableCommands();
            service.DisplayMessage("Enter a city");

            var userInput = "";
            while (userInput != "e")
            {
                userInput = Console.ReadLine();

                if (!service.ValidateInput(userInput)) continue;
                userInput = userInput.ToLower();

                if (service.CheckAndProcessForCommand(userInput)) continue;

                service.ProcessGetWeather(userInput);
            }
            Console.ReadLine();
        }
    }
}
